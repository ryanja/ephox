
//log object contains all methods to log to 
let log = {

    onToggleSelect(row, cell, selected) {
        let log = document.getElementById("log");
        if (selected) {
            log.innerHTML === "" ? log.innerHTML = `cell (${row}, ${cell}) selected.` :
                log.innerHTML += `<br />cell (${row}, ${cell}) selected.`;
            return (true);
        }
        else {
            log.innerHTML += `<br />cell (${row}, ${cell}) unselected.`;
            return (true);
        }
    },

    onToggleRowSelect(row, selected) {
        let log = document.getElementById("log");
        if (selected) {
            log.innerHTML === "" ? log.innerHTML = `row ${row} selected.` :
                log.innerHTML += `<br />row ${row} selected.`;
            return (true);
        }
        else {
            log.innerHTML += `<br />row ${row} unselected.`;
            return (true);
        }
    },

    onDelete(row, cell) {
        let log = document.getElementById("log");
        log.innerHTML === "" ? log.innerHTML = `cell (${row}, ${cell}) deleted.` :
            log.innerHTML += `<br />cell (${row}, ${cell}) deleted.`;
        return (true);
    }
};

export { log };