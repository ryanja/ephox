import { log } from "./log";
let state = (function () {
    let stateObj = new Map();
    return {
        getState() {
            return stateObj.values();
        },
        //only place stateObj is mutated
        //better work around aiming for immutability?
        setRightClickState(row, cell) {
            let currrentCell = { row, cell };
            if (stateObj.has(`${row},${cell}`)) {
                stateObj.delete(`${row},${cell}`);
                log.onToggleSelect(row, cell, false);
            } else {
                stateObj.set(`${row},${cell}`, currrentCell);
                log.onToggleSelect(row, cell, true);
            }
        },
        setClickState(row, cell) {
            let currrentCell = { row, cell };
            if (stateObj.has(`${row},${cell}`)) {
                this.resetState();
                log.onToggleSelect(row, cell, false);
            } else {
                this.resetState();
                stateObj.set(`${row},${cell}`, currrentCell);
                log.onToggleSelect(row, cell, true);
            }
        },
        setDoubleClickState(eachRow, cellIndex) {
            let row = eachRow.rowIndex;
            let cells = eachRow.cells;
            if (!stateObj.has(`${row},${cellIndex}`)) {
                this.resetState();
                Array.from(cells).map(eachCell => {
                    let cell = eachCell.cellIndex;
                    let currrentCell = { row, cell };
                    stateObj.set(`${row},${cell}`, currrentCell);
                });
                log.onToggleRowSelect(row, true);
            }else{
                this.resetState();
                log.onToggleRowSelect(row, false);
            }
        },
        resetState() {
            stateObj = new Map();
        }
    };
})();
export default state;