import { createTable } from "./iframe";
import { style } from "./style";
import state from "./state";
//first thing to do to listen on DOM creation finished
//and add event listener to button
window.onload = function () {
    let button = document.getElementById("button");
    button.addEventListener("click", buttonClicked);
};
//make iframe div visible from hidden after button click with right values.
function buttonClicked() {
    let iframe_div = document.getElementById("iframe_div");
    let rows = document.getElementById("rows");
    let columns = document.getElementById("columns");
    //could use coercion here when comparing values to zero!
    let rows_number = parseInt(rows.value);
    let columns_number = parseInt(columns.value);
    if (rows_number > 0 && columns_number > 0) {
        style.makeItVisible(iframe_div);
        if (createTable(rows_number, columns_number)) {
            rows.value = "";
            columns.value = "";
            if (state.getState().length !== 0) {
                state.resetState();
            }
            return (true);
        }
    }
    return (false);
}