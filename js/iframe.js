import { events } from "./eventListeners";
import { style } from "./style";

//main function to create the table inside iframe
// it makes call to addiframeTable and createTableRowsColumns
export let tableFactory;
export let setTable;
export function createTable(rows, columns) {
    let iframe = document.getElementById('iframe');
    let iframe_document = (iframe.contentWindow || iframe.contentDocument);
    if (iframe_document.document) iframe_document = iframe_document.document;
    let table = addiframeTable(iframe_document);
    if (createTableRowsColumns(table, rows, columns)) {
        iframe_document.open();
        iframe_document.write(table.outerHTML);
        iframe_document.close();
        iframe.onload = addEventsToiframe(iframe_document);
        tableFactory = function () {
            return table.cloneNode(true);
        };
        setTable = function (newTable) {
            table = newTable;
        };
        return (true);
    }
    return (false);
}

//once finished with creating table.
// add event listeners for click dbclick rightclick and delete button.
export function addEventsToiframe(iframe_document) {
    iframe_document.body.addEventListener("keyup", events.onDeleteButton);
    iframe_document.addEventListener("mousemove", events.onMouseMove);
    iframe_document.addEventListener("mouseout", events.clearCoords);
    iframe_document.addEventListener("contextmenu", events.onRightClick);
    let iframe_table = iframe_document.getElementById("iframe_table");
    iframe_table.addEventListener("dblclick", events.tableDblClicked);
    iframe_table.addEventListener("click", events.onCellClicked);
    iframe_table.addEventListener("contextmenu", events.onCellRightClck);
    return (true);
}

//create table element programatically and add style to it.
function addiframeTable(iframe_document) {
    let table = iframe_document.createElement("table");
    table.setAttribute("id", "iframe_table");
    style.tableStyle(table);
    return (table);
}

//create table rows and clomuns and add style to them.
//just two simple for loops!
function createTableRowsColumns(table, rows, columns) {
    for (let i = 0; i < rows; i++) {
        let row = table.insertRow(i);
        style.tableRowStyle(row);
        for (let j = 0; j < columns; j++) {
            let cell = row.insertCell(j);
            style.tableCellStyle(cell);
        }
    }
    return (true);
}