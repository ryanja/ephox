//styling file. Object.assign is used to pass the original object and overwrite its properties with folowing object
//Object.assign return brand new object so can be use to keep objects immutable.
//that was my first intention to use it here but could not replace element style object with new object. 
let style = {

    makeItVisible(el) {
        return Object.assign(el.style, { visibility: "visible" });
    },
    tableStyle(table) {
        return Object.assign(table.style, { width: "100%", height: "100%", border: "1px solid green", tableLayout:"fixed" });
    },
    tableRowStyle(cell) {
        return Object.assign(cell.style, {border: "1px solid black"});
    },
    tableCellStyle(cell) {
        return Object.assign(cell.style, { width: "15px", height: "15px", border: "1px solid black" });
    },
    onMouseMoveStyle(el, x, y) {
        return Object.assign(el.style, {
            visibility: "visible", color: "#FF00FF",
            left: (x + 150) + "px", top: (y + 300) + "px"
        });
    },
    onClearCoords(el) {
        return Object.assign(el.style, { visibility: "hidden" });
    },
    onClick(el) {
        return Object.assign(el.style, { backgroundColor: "rgb(0, 0, 0)" });
    },
    onDelete(el) {
        return Object.assign(el.style, { visibility: "hidden" });
    }
};
export { style };