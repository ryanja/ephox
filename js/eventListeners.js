import { style } from "./style";
import { onClickButton, onDeleteButton } from "./utility";
import { tableFactory } from "./iframe";
import state from "./state";

// all iframe event listeners are handled here.
let events = {
    //onmouse move over iframe and show the grid.
    onMouseMove(e) {
        let x = e.clientX;
        let y = e.clientY;
        let coordinates = `( ${x}, ${y} )`;
        let coords = document.getElementById("coords");
        style.onMouseMoveStyle(coords, x, y);
        coords.innerHTML = coordinates;
        return (true);
    },
    //clear coords on mouse move out of iframe.
    clearCoords() {
        let coords = document.getElementById("coords");
        style.onClearCoords(coords);
        return (true);
    },
    //supress mouse right click inside iframe.
    onRightClick(event) {
        event.preventDefault !== undefined && event.preventDefault();
    },
    //toggle cell color on right click without changing other cells.
    onCellRightClck(e) {
        e.preventDefault !== undefined && e.preventDefault();
        let cellIndex = e.target.cellIndex;
        let rowIndex = e.target.parentElement.rowIndex;
        if (rowIndex !== undefined && cellIndex !== undefined) {
            state.setRightClickState(rowIndex, cellIndex);
            let table = tableFactory();
            onClickButton(table);
        }
    },
    onCellClicked(e) {
        let check = e.target.getAttribute("single_double");
        check === null ? e.target.setAttribute("single_double", 1) : e.target.setAttribute("single_double", 2);
        setTimeout(() => {
            let check = e.target.getAttribute("single_double");
            if (check == 1) {
                let rowIndex = e.target.parentElement.rowIndex;
                let cellIndex = e.target.cellIndex;
                if (rowIndex !== undefined && cellIndex !== undefined) {
                    state.setClickState(rowIndex, cellIndex);
                    let table = tableFactory();
                    onClickButton(table);
                }
            }
            e.target.removeAttribute("single_double");
        }, 200);
    },
    //using loopOverDbclick from utility to loop over all elements toggle their color 
    //also highlight the focused row.
    tableDblClicked(e) {
        let rowIndex = e.target.parentElement.rowIndex;
        let cellIndex = e.target.cellIndex;
        if (rowIndex !== undefined && cellIndex !== undefined) {
            let row = e.target.parentElement;
            state.setDoubleClickState(row, cellIndex);
            let table = tableFactory();
            onClickButton(table);
        }
    },
    //using loopOverDeleteButton from utility to loop over all elements and delete highlighted cells.
    onDeleteButton(e) {
        if (e.keyCode == 8 || e.keyCode == 46) {
            let table = tableFactory();
            if (onDeleteButton(table)) {
                return (true);
            }
        }
    }
};
export { events };