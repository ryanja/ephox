import { style } from "./style";
import { log } from "./log";
import { addEventsToiframe, setTable } from "./iframe";
import state  from "./state";

export function onClickButton(table) {
    let currentState = state.getState();
    Array.from(currentState, obj => {
        let table_row = table.rows[obj.row];
        let cell = table_row.cells[obj.cell];
        style.onClick(cell);
    });
    replaceTable(table);
    return (true);
}

export function onDeleteButton(table) {
    let currentState = state.getState();
    Array.from(currentState, obj => {
        let table_row = table.rows[obj.row];
        let cell = table_row.cells[obj.cell];
        style.onDelete(cell);
        log.onDelete(table_row.rowIndex, cell.cellIndex);
    });
    setTable(table);
    replaceTable(table);
    state.resetState();
    return (true);
}

function replaceTable(table) {
    let iframe = document.getElementById('iframe');
    let iframe_document = (iframe.contentWindow || iframe.contentDocument);
    if (iframe_document.document)
        iframe_document = iframe_document.document;
    iframe_document.open();
    iframe_document.write(table.outerHTML);
    iframe_document.close();
    iframe.onload = addEventsToiframe(iframe_document);
    return (true);
}